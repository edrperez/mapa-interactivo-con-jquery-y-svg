$(document).ready(function () {

    var municipios, escalas, datos;
    
    function recuperarJsons(respuestaM, respuestaE, respuestaD){
        municipios = respuestaM;
        escalas = respuestaE;
        datos = respuestaD;
    }
    
    $.when(
        $.ajax({
            url: "municipios.json",
            dataType: 'json',
            type: 'GET'}),
        $.ajax({
            url: "escalas.json",
            dataType: 'json',
            type: 'GET'}),
        $.ajax({
            url: "datos.json",
            dataType: 'json',
            type: 'GET'}),
        $.ajax({
            url: "limites_municipales_codigos.svg",
            dataType: 'html',
            type: 'GET'})
        )
        .done(function(municipios, escalas, datos, mapa){
            recuperarJsons(municipios[0], escalas[0], datos[0]);
            $(".mapa").html(mapa[0]);
            rellenarMapa();
            generarLeyenda();
        });    
    
    function rellenarMapa() {
        for (var k in datos) {
            $('#mun' + k)
                    .css({'fill': '' + escalas[datos[k]].color + ''})
                    .data('area', [k, datos[k]]);
        }

        $('.mun').mouseover(function (e) {
            var datos_municipio = $(this).data('area');
            if (datos_municipio !== undefined) {
                $('<div class="panel_info">' +
                        municipios[datos_municipio[0]]["nombre"] + '<br>' +
                        'Escala: ' + escalas[datos_municipio[1]].nombre + 
                        '</div>'
                        )
                        .appendTo('body');
            }
        })
        .mouseleave(function () {
            $('.panel_info').remove();
        })
        .mousemove(function (e) {
            var mouseX = e.pageX, mouseY = e.pageY;

            $('.panel_info').css({
                top: mouseY - 50,
                left: mouseX - ($('.panel_info').width() / 2)
            });
        }).on('click tap touchstart', function (e) {
            var datos_municipio = $(this).data('area');
            
            if (datos_municipio !== undefined) {
                $('#movil').html('<strong>' + municipios[datos_municipio[0]]["nombre"] + '</strong><br>' +
                'Escala: ' + escalas[datos_municipio[1]].nombre);
            }
        });
    }
    
    function generarLeyenda(){
        var tabla_leyenda = $("#leyenda_escala").find('tbody');
        for (var k in escalas) {
            var fila = tabla_leyenda.append($('<tr>'));
            fila.append($('<td>').text(escalas[k]["nombre"]));
            fila.append($('<td class="color">').css({'background-color': '' + escalas[k]["color"] + ''}));
        }
    }
});